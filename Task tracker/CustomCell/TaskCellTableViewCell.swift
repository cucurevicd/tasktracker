//
//  TaskCellTableViewCell.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class TaskCellTableViewCell: SBGestureTableViewCell {

    
    //Properties
    
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    var originalFrame : CGRect?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(task: Task, currentDate : NSDate){
        
        self.originalFrame = self.frame
        self.taskName.text = task.title
        self.dueDateLabel.text = Util.stringFromDate(task.dueDate!, format: "MMM dd YYYY")
        
        self.daysLeftLabel.text = "\(Util.calculateDays(currentDate, endDate: task.dueDate!))"
        
        //Set status image
        if task.status != Status.Unresolve.rawValue{
            
            self.statusImage.image = UIImage(named: task.status!)
        }
        else{
            self.statusImage.image = nil
        }

        //Can change task status only if status is unresolved and if due date is greater than current date
        
        if (currentDate.isGreaterThanDate(task.dueDate!) || task.status != Status.Unresolve.rawValue){
            
            self.firstRightAction = nil
            self.firstRightAction = nil
            self.leftSideView.iconImageView.backgroundColor = UIColor.clearColor()
            self.rightSideView.iconImageView.backgroundColor = UIColor.clearColor()
            self.leftSideView.iconImageView.image = nil
            self.rightSideView.iconImageView.image = nil
        }
        else{
            if self.firstLeftAction == nil {
                
                self.firstLeftAction = SBGestureTableViewCellAction(icon: UIImage(named: Status.Resolve.rawValue)!, color: UIColor.clearColor(), fraction: 0.4, didTriggerBlock: { (tableView, cell) -> () in
                    task.status = Status.Resolve.rawValue
                    self.statusImage.image = UIImage(named: Status.Resolve.rawValue)
                    DELEGATE.saveContext()
                    self.animateBackToOriginalPosition(self.frame)
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(ReloadTable, object: self)
                })
            }
            
            if self.firstRightAction == nil{
            
                self.firstRightAction = SBGestureTableViewCellAction(icon: UIImage(named: Status.CantResolve.rawValue)!, color: UIColor.clearColor(), fraction: 0.4, didTriggerBlock: { (tableView, cell) -> () in
                    task.status = Status.CantResolve.rawValue
                    self.statusImage.image = UIImage(named: Status.CantResolve.rawValue)
                    DELEGATE.saveContext()
                    self.animateBackToOriginalPosition(self.frame)
                    NSNotificationCenter.defaultCenter().postNotificationName(ReloadTable, object: self)
                })
                
                self.leftSideView.iconImageView.backgroundColor = UIColor.clearColor()
                self.rightSideView.iconImageView.backgroundColor = UIColor.clearColor()
            }
        }
    }
    
    func animateBackToOriginalPosition(lastFrame : CGRect){
        
        UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
            self.frame = self.originalFrame!
            }, completion: nil)
    }
}
