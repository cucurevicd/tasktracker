//
//  LeftCommentView.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/28/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

protocol NoteSetDelegate{
    
    func noteSet(note : String)
}

class LeftCommentView: BaseCustomView {

    @IBOutlet weak var noteTextView: UITextView!
    
    var delegate : NoteSetDelegate?

    override func awakeFromNib() {
        
        self.noteTextView.becomeFirstResponder()
    }
    
    func configView(superView : UIView, note : String){
        
        // Dimiss view
        self.dismissView = UIView(frame: MAIN_SCREEN!.bounds)
        self.dismissView.backgroundColor = UIColor.blackColor()
        self.dismissView.alpha = 0.6
        
        // Gesture recognizer dismiss
        self.dismiss = UITapGestureRecognizer(target: self, action: Selector("hideView"))
        
        self.dismissView.addGestureRecognizer(self.dismiss)
        
        self.width = (MAIN_SCREEN?.bounds.width)! * 2/3
        self.height = (MAIN_SCREEN?.bounds.height)! * 3/5
        self.frame = CGRectMake(0, 0, self.width, self.height)
        self.center = superView.center
        
        superView.addSubview(self.dismissView)
        superView.addSubview(self)
        
        self.noteTextView.text = note
        self.noteTextView.font = UIFont(name: "AmsiPro-Regular", size: 14)
        self.noteTextView.textColor = UIColor.appTextColor()
    }

    @IBAction func okButtonAction(sender: UIButton) {
        
        if noteTextView.text != nil{
            self.delegate?.noteSet(self.noteTextView.text)
        }
        
        self.hideView()
    }
}
