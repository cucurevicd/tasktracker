//
//  BaseCustomView.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class BaseCustomView: UIView {

    var dismiss : UITapGestureRecognizer!
    var width : CGFloat!
    var height : CGFloat!
    var dismissView : UIView!
    var viewFrame : CGRect?
    
    override func awakeFromNib() {
        
        // Dimiss view
        self.dismissView = UIView(frame: MAIN_SCREEN!.bounds)
        self.dismissView.backgroundColor = UIColor.blackColor()
        self.dismissView.alpha = 0.6
        
        // Gesture recognizer dismiss
        self.dismiss = UITapGestureRecognizer(target: self, action: Selector("hideView"))
        
        self.dismissView.addGestureRecognizer(self.dismiss)
        
        if viewFrame != nil{
            self.frame = CGRectMake(0, 0, self.width, self.height)
            self.center = MAIN_SCREEN!.center
        }
        
        MAIN_SCREEN!.addSubview(self.dismissView)
        
        MAIN_SCREEN!.addSubview(self)
    }
    
    func hideView(){
        
        self.dismissView.removeFromSuperview()
        self.removeFromSuperview()
    }

}
