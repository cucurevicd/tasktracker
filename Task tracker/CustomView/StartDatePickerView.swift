//
//  StartDatePickerView.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

protocol StartDateSelectedDelegate {
    func startDateSelected(date : NSDate)
}

class StartDatePickerView: BaseCustomView {

    //Properties
    @IBOutlet weak var pickerDate: UIDatePicker!
    var delegate : StartDateSelectedDelegate?
    var pickerMode = false
    
    //View cycle
    override func awakeFromNib() {
        
        if self.pickerMode == false{
            self.width = (MAIN_SCREEN?.bounds.width)! * 5/6
            self.height = (MAIN_SCREEN?.bounds.height)! * 3/4
            self.viewFrame = CGRectMake(0, 0, self.width, self.height)
            
            super.awakeFromNib()
         
            self.pickerMode = true
        }
    }

    
    
    //Button action
    @IBAction func okButtonAction(sender: UIButton) {
        
        delegate?.startDateSelected(self.pickerDate.date)
        self.hideView()
    }
    
    override func hideView() {
        delegate?.startDateSelected(self.pickerDate.date)
        self.pickerMode = false
        super.hideView()
    }
}
