//
//  LoadingView.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class LoadingView: BaseCustomView {

    //MARK: - Properties
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    
    
    //MARK: - View cycle
    
    override func awakeFromNib() {
        
        self.width = MAIN_SCREEN!.bounds.width
        self.height = MAIN_SCREEN!.bounds.height
        self.viewFrame = CGRectMake(0, 0, self.width, self.height)
        
        self.activityIndicator.startAnimating()
        
        super.awakeFromNib()
    }
    
    override func hideView() {
        self.activityIndicator.stopAnimating()
        self.image.image = UIImage(named: "wink")
        self.performSelector(Selector("superHide"), withObject: self , afterDelay: 0.6)
    }
    
    func superHide(){
        super.hideView()
    }
}
