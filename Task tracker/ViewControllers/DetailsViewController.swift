//
//  DetailsViewController.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, NoteSetDelegate, UIGestureRecognizerDelegate {

    
    //MARK: - Properties
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel: UITextView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var targetDayLabel: UILabel!
    
    @IBOutlet weak var viewWithButtons: UIView!
    @IBOutlet weak var noteButton: UIButton!
    
    var task : Task?
    var selectedDate : NSDate?
    var currentDate : NSDate?
    
    var noteView : LeftCommentView?
    
    //Tmp values
    var selectedStatus : String?{
        didSet{
            self.statusLabel.text = selectedStatus
            if selectedStatus == Status.Resolve.rawValue{
                self.statusLabel.textColor = UIColor.appGreenColor()
                targetDayLabel.textColor = UIColor.appGreenColor()
                daysLeftLabel.textColor = UIColor.appGreenColor()
                taskNameLabel.textColor = UIColor.appGreenColor()
            }
            else if selectedStatus == Status.CantResolve.rawValue{
                self.statusLabel.textColor = UIColor.appRedColor()
                targetDayLabel.textColor = UIColor.appRedColor()
                daysLeftLabel.textColor = UIColor.appRedColor()
                taskNameLabel.textColor = UIColor.appRedColor()
            }
        }
    }
    var setNote : String?
    
    //MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set UI
        self.setUI()
        
        self.navigationController?.interactivePopGestureRecognizer!.enabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UI design
    func setUI(){
        
        self.taskNameLabel.text = task?.title
        
        self.targetDayLabel.text = Util.stringFromDate(task!.dueDate!, format: "MMM dd YYYY")
        self.daysLeftLabel.text = "\(Util.calculateDays(currentDate!, endDate: task!.dueDate!))"
        
        self.taskDescriptionLabel.text = task!.desc
        
        self.taskDescriptionLabel.font = UIFont(name: "AmsiPro-Regular", size: 15)
        self.taskDescriptionLabel.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        self.taskDescriptionLabel.textColor = UIColor.appTextColor()
        
        self.selectedStatus = task?.status
        
        //Set status image
        if task!.status != Status.Unresolve.rawValue{
            self.viewWithButtons.hidden = true
            self.statusImage.image = UIImage(named: (task?.status!)!)
        }
        else{
            self.viewWithButtons.hidden = false
            self.statusImage.image = nil
        }
        
        //If due date is less than current date disable status buttons
        if currentDate!.isGreaterThanDate(task!.dueDate!){
            self.viewWithButtons.hidden = true
        }
        
        self.noteButton.hidden = self.task?.note == ""
    }

    
    //MARK: - Button action
    
    @IBAction func backButtonAction(sender: UIButton) {
        
        //Store changes on task
        if selectedStatus != nil{
            task?.status = selectedStatus
        }
        
        if setNote != nil{
            task?.note = setNote
        }
        
        DELEGATE.saveContext()
        
        //Back to main view controller
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    @IBAction func resolveButtonAction(sender: UIButton) {
        
        self.selectedStatus = Status.Resolve.rawValue
        alertView()
    }
    
    @IBAction func cantResolveButtonAction(sender: UIButton) {

        self.selectedStatus = Status.CantResolve.rawValue
        alertView()
    }
    
    @IBAction func showNoteButtonAction(sender: UIButton) {
        
        self.noteView = NSBundle.mainBundle().loadNibNamed("LeftCommentView", owner: self, options: nil)[0] as? LeftCommentView
        self.noteView?.configView(self.view, note: (task?.note)!)
        
        self.noteView?.delegate = self
    }
    
    //MARK: - Left note
    
    func alertView(){
        
        
        let alertController = UIAlertController(title: title, message: STRING("Do you want to left comment?"), preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: STRING("NO"), style: .Cancel) { (action) in
            
            alertController.removeFromParentViewController()
        }
        
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: STRING("YES"), style: .Default) { (action) in
            
            self.noteView = NSBundle.mainBundle().loadNibNamed("LeftCommentView", owner: self, options: nil)[0] as? LeftCommentView
            self.noteView?.configView(self.view, note: (self.task?.note)!)
            
            self.noteView?.delegate = self
        }
        
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: - Note delgate
    func noteSet(note: String) {
        
        self.noteButton.hidden = false
        self.task?.note = note
    }
    
    //MARK: - Go back on swipe
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
