//
//  MainViewController.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, StartDateSelectedDelegate, UIGestureRecognizerDelegate {
    
    //MARK: - Properties
    
    var secretGesture: UILongPressGestureRecognizer!
    @IBOutlet weak var nextDayBtn: UIButton!
    @IBOutlet weak var previousDayBtn: UIButton!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tableView: SBGestureTableView!
    @IBOutlet weak var secretGestureView: UIView!
    @IBOutlet weak var noTaskView: UIView!

    let cellIdentifier = "TaskCell"
    var startDatePicker : StartDatePickerView?
    var selectedTask : Task?
    
    var dataArray = [Task]()
    var selectedDate : NSDate?{
        didSet{
            if selectedDate == startDate{
                self.dayLabel.text = STRING("Today")
            }
            else{
                self.dayLabel.text = Util.stringFromDate(selectedDate!, format: nil)
            }
            
            //Get tasks for selected date
            self.getTasksForDay(selectedDate!)
        }
    }
    
    var startDate : NSDate?{
        didSet{
            
            self.selectedDate = startDate
        }
    }
    
    //MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        //Crete secret gesture for slecting start date
        self.secretGesture = UILongPressGestureRecognizer(target: self, action: Selector("openDatePicker:"))
        self.secretGesture.minimumPressDuration = 2.0
        self.secretGestureView.addGestureRecognizer(self.secretGesture)
        self.secretGesture.delegate = self
        
        //For start. User can change it on secret gesture, becasue of tasks days from api
        self.startDate = NSDate()
        
        //Set notificaton when all tasks are loaded
        NSNotificationCenter.defaultCenter().addObserverForName(TASKS_LOADED, object: nil, queue: NSOperationQueue.mainQueue()) { (notificaton) -> Void in
            
            self.selectedDate = self.startDate
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(ReloadTable, object: nil, queue: NSOperationQueue.mainQueue()) { (notificaton) -> Void in
            
            //Get tasks for selected date
            self.getTasksForDay(self.selectedDate!)
        }

        //Get all tasks
        APIMethods.getTasks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table view
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.dataArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        (cell as? TaskCellTableViewCell)?.configCell(self.dataArray[indexPath.row], currentDate: startDate!)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedTask = self.dataArray[indexPath.row]
        
        self.performSegueWithIdentifier("DetailsSegue", sender: self)
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
       
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    
    //MARK: - Get tasks for selected day
    
    func getTasksForDay(date : NSDate){
        
        self.dataArray = [Task]()
        let allTasks = DBManager.getTasksForDay(date)
        self.dataArray = Util.sortByPriority(allTasks)
        
        //Check does exsist tasks for tommorow and day before
//        self.nextDayBtn.enabled = DBManager.getTasksForDay(Util.addDay(date)).count != 0
//        self.previousDayBtn.enabled = DBManager.getTasksForDay(Util.minusDay(date)).count != 0
        
        //Reload table view
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.tableView.reloadData()
        }
        
        self.noTaskView.hidden = self.dataArray.count != 0
    }

    //MARK: - Button actions
    
    @IBAction func previousDayButtonAction(sender: UIButton) {

        self.selectedDate = Util.minusDay(selectedDate!)
    }

    @IBAction func nextDayButtonAction(sender: UIButton) {
        
        self.selectedDate = Util.addDay(selectedDate!)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "DetailsSegue"{
            (segue.destinationViewController as? DetailsViewController)!.task = self.selectedTask
            (segue.destinationViewController as? DetailsViewController)!.selectedDate = self.selectedDate
            (segue.destinationViewController as? DetailsViewController)!.currentDate = self.startDate
        }
    }

    //MARK: - Gesture recognizer
    func openDatePicker(sender: UILongPressGestureRecognizer) {
        
        if startDatePicker == nil{
            self.startDatePicker = NSBundle.mainBundle().loadNibNamed("StartDatePicker", owner: self, options: nil)[0] as? StartDatePickerView
            
            self.startDatePicker?.delegate = self
        }
    }
    
    //MARK: - Start date picker delegate
    func startDateSelected(date: NSDate) {
        self.startDatePicker = nil
        self.startDate = date
    }
}
