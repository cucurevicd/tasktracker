//
//  DBManager.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import CoreData

class DBManager: NSObject {
    
    //MARK: - GET
    
    static func getAllTasks() -> [Task]{
        
        return (self.fetch("Task", predicate: nil) as? [Task])!
    }
    
    static func getTasksForDay(date : NSDate) -> [Task]{
        
        let predicate = NSPredicate(format:"%@ == targetDate OR (%@ > targetDate AND %@ <= dueDate)", date, date, date)
        return self.fetch("Task", predicate: predicate) as! [Task]
    }
    
    //MARK: - SET
    
    static func setAllTasks(responseArray : [NSDictionary]){
        
        
        let tasks = DBManager.getAllTasks()
        for dict in responseArray{
            
            var unique = true
            for task in tasks{
                if task.id == dict.objectForKey("id") as? String{
                    unique = false;
                    break;
                }
            }
            if unique{
                saveTask(dict)
            }

        }
        
        //Post notificaton as signal to get from date base all data on main screen
        NSNotificationCenter.defaultCenter().postNotificationName(TASKS_LOADED, object: self)
    }
    
    static func saveTask(dict : NSDictionary){
        
        let entity = NSEntityDescription.entityForName("Task", inManagedObjectContext: DELEGATE.managedObjectContext)
        let taskObj = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: DELEGATE.managedObjectContext) as? Task
        
        taskObj?.createTask(dict)
        
        DELEGATE.saveContext()
        
    }

    
    //MARK:- General method
    static func fetch(entityName:String, predicate : NSPredicate?) ->NSArray{
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        if predicate != nil{
            fetchRequest.predicate = predicate
        }
        do{
            let fetchedUser = try DELEGATE.managedObjectContext.executeFetchRequest(fetchRequest)
            return fetchedUser
        }
        catch{
            fatalError("Problem with fatching user \(error)")
        }
    }
}
