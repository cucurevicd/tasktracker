//
//  Task.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/23/16.
//  Copyright © 2016 house. All rights reserved.
//

import Foundation
import CoreData

class Task: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    func createTask(dict : NSDictionary){
        
        self.id = dict["id"] as? String ?? ""
        self.title = dict["title"] as? String ?? ""
        
        let dateFormat = "YYYY-MM-dd HH:mm:ss"
        let targetDateString = dict["TargetDate"] as? String ?? ""
        self.targetDate = Util.fullDateFromString(targetDateString, dateFormat: dateFormat)
        let dueDateString = dict["DueDate"] as? String ?? ""
        self.dueDate = Util.fullDateFromString(dueDateString, dateFormat: dateFormat)
        self.desc = dict["Description"] as? String ?? ""
        self.priority = dict["Priority"] as? NSInteger ?? 0
        
        /* Status : 0 - Unresolve
                    1 - Resolve
                    2 - Can'tResolve
        */
        self.status = Status.Unresolve.rawValue
        self.note = ""
    }

}
