//
//  Task+CoreDataProperties.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/23/16.
//  Copyright © 2016 house. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var id: String?
    @NSManaged var title: String?
    @NSManaged var targetDate: NSDate?
    @NSManaged var dueDate: NSDate?
    @NSManaged var desc: String?
    @NSManaged var priority: NSNumber?
    @NSManaged var status: String?
    @NSManaged var note: String?

}
