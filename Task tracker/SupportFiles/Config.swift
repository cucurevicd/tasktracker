//
//  Config.swift
//  auweather
//
//  Created by Dusan Cucurevic on 11/29/15.
//  Copyright © 2015 tomfusion. All rights reserved.
//

import Foundation
import UIKit


// Notifications
let TASKS_LOADED = "tasksLoaded"
let ReloadTable = "ReloadTable"

/// Macros
let MAIN_SCREEN	= UIApplication.sharedApplication().keyWindow
let DELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate

enum Status: String {
    case Unresolve = "Unresolved",
        Resolve = "Resolve",
        CantResolve = "Can't resolve"
}

//Common string
let ERROR = NSLocalizedString("Error", comment: "Error")
let OK = NSLocalizedString("OK", comment: "ok")
let YES = NSLocalizedString("YES", comment: "yes")
let NO = NSLocalizedString("NO", comment: "no")
let CANCEL = NSLocalizedString("Cancel", comment: "cancel")
let NO_INTERNET = NSLocalizedString("No internet connection", comment: "No internet connection")
let NETWORK_ERROR = NSLocalizedString("Network error", comment: "Network error")

func STRING(string: String) -> String{
    return NSLocalizedString(string, comment: "")
}