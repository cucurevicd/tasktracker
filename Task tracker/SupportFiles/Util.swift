//
//  Util.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/23/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    // MARK:- Date formater
    
    /**
    Return NSDate from string
    
    - parameter dateString: string date
    - parameter dateFormat: format of date
    
    - returns: date in NSDate format
    */
    static func fullDateFromString(dateString : String, dateFormat : String) -> NSDate{
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = dateFormat
        
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    /**
     Create string from NSDate. If there is not format parameter, use local settings
     
     - parameter date:   NSDate format
     - parameter format: String format of 
     
     - returns: return string of date
     */
    static func stringFromDate(date : NSDate, format : String?) ->String{
        
        let dateFormatter = NSDateFormatter()
        let formatLocale = NSDateFormatter.dateFormatFromTemplate("MM dd", options: 0, locale: NSLocale.currentLocale())
        dateFormatter.dateFormat = format ?? formatLocale
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        return dateFormatter.stringFromDate(date)
    }
    
    
    static func addDay(date : NSDate) -> NSDate{
        let nextDay = NSCalendar.currentCalendar().dateByAddingUnit(
            .Day,
            value: 1,
            toDate: date,
            options: NSCalendarOptions(rawValue: 0))
        
        return nextDay!
    }
    
    static func minusDay(date : NSDate) -> NSDate{
        
        let dayBefore = NSCalendar.currentCalendar().dateByAddingUnit(
            .Day,
            value: -1,
            toDate: date,
            options: NSCalendarOptions(rawValue: 0))
        
        return dayBefore!
    }
    
    static func calculateDays(startDate : NSDate, endDate : NSDate) -> NSInteger{
        
        let cal = NSCalendar.currentCalendar()
        let unit = NSCalendarUnit.Day
        
        let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: NSCalendarOptions(rawValue: 0))
        
        return components.day
    }
    
    
    //MARK: - Alert view
    
    /**
    Create alert controller
    
    - parameter title:   Title of alert controller
    - parameter message: message
    */
    static func alert(title: String,message: String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: OK, style: .Cancel) { (action) in
            
            alertController.removeFromParentViewController()
        }
        
        alertController.addAction(cancelAction)
                
        UIApplication.topViewController()!.presentViewController(alertController, animated: true) {
            
        }
    }
    
    
    //MARK:- Sort
    static func sortByPriority(array : [Task]) -> [Task]{
        
        let sortedArrayDate = array.sort({ $0.targetDate!.compare($1.targetDate!) == NSComparisonResult.OrderedAscending })
        
        let sortedArray = sortedArrayDate.sort({ $0.priority!.compare($1.priority!) == NSComparisonResult.OrderedAscending })
        return sortedArray
    }
}



//MARK: - Application

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

//MARK: - Colors
extension UIColor{
    
    class func appRedColor()->UIColor{
        
        return UIColor(red: 239.0/255.0, green: 75.0/255, blue: 94.0/255, alpha: 1)
    }
    
    class func appGreenColor() -> UIColor {
        return UIColor(red: 15.0/255, green: 116.0/255, blue: 101.0/255, alpha: 1)
    }
    
    class func appYellowColor() -> UIColor {
        return UIColor(red: 255.0/255, green: 222.0/255, blue: 97.0/255, alpha: 1)
    }
    
    class func appTextColor() -> UIColor{
        return UIColor(red: 90.0/255, green: 83.0/255, blue: 85.0/255, alpha: 1)
    }
}

//MARK: - Date
extension NSDate {
    
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
}
