//
//  APIMethods.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/24/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit

class APIMethods: NSObject {
    
    static func getTasks(){
        BaseAPI.execute(.GET, service: "", params: nil) { (result) -> Void in
            print(result)
            DBManager.setAllTasks(result!)
        }
    }

}
