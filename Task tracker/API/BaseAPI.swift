//
//  BaseAPI.swift
//  Task tracker
//
//  Created by Dusan Cucurevic on 2/23/16.
//  Copyright © 2016 house. All rights reserved.
//

import UIKit
import Alamofire

let BASE_URL = "https://demo8035300.mockable.io/tasks"
public enum HTTPMethod: String {
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

class BaseAPI: NSObject {

    // Execute API call
    class func execute(method: HTTPMethod, service: String, params: [String : AnyObject]?, completion: (result: [NSDictionary]?) -> Void) {
        
        //Start animation view
        let animationView = NSBundle.mainBundle().loadNibNamed("LoadingView", owner: self, options: nil)[0] as? LoadingView
        
        // Create URL
        let url = String(format: "%@%@", BASE_URL, service)
        
        // Select http method
        var httpMethod = Method.GET
        
        if method == HTTPMethod.GET {
            httpMethod = Method.GET
        } else if method == HTTPMethod.POST {
            httpMethod = Method.POST
        } else if method == HTTPMethod.PUT {
            httpMethod = Method.PUT
        }
        
        
        // Alamofire async request
        
        Alamofire.request(httpMethod, url, parameters: params, encoding: .JSON, headers: ["Content-Type" : "application/json"]).responseJSON { (response) -> Void in
            
            //Hide animation view
            animationView!.hideView()
            
            
            //Handle error

            // Error code -999 is for canceled request
            if response.result.error != nil{
                
                if response.result.error!.code == -999{
                    Util.alert(ERROR, message: STRING("Request has bean canceled"))
                    return
                }
                    // No internet connection
                else if response.result.error!.code == -1009 {
                    Util.alert(ERROR, message: NO_INTERNET)
                    return
                }
                else{
                    Util.alert(ERROR, message:(response.result.error?.localizedDescription)!)
                    return
                }
            }
            
            //Handle response
            if response.result.isSuccess{
                if let result = response.result.value as? NSDictionary{
                    
                    //Check status 
                    let status = result.objectForKey("status") as? NSInteger
                    
                    if status == 200{
                        
                        completion(result: result.objectForKey("tasks ") as? [NSDictionary])
                    }
                    else{
                        Util.alert(ERROR, message: NETWORK_ERROR)
                    }
                }
            }
        }
    }
}
